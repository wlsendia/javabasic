package com.wlsendia.javabasic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaBasicApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaBasicApplication.class, args);

        boolean myBox1 = true;
        System.out.println(myBox1);

        char myBox2 = 'A';
        Character myBox222 = 'A';
        String myBox22 = "A";

        byte myBox3 = 1;
        Byte myBox33 = 1;

        short myBox4 = 2;
        int myBox5 = 10;
        long mtBox6 = 15;

        float myB0x7 = 11.1f;
        double myBox8 = 11.1;
    }
    /*
    git add .
    git commit -m "주석 추가, 1월 10일"
    git push
    */
}
